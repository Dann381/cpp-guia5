# Guía 5 - Unidad II

### Descripción
Este algoritmo consiste en la creación de un árbol balanceado que recibe indentificadores (strings) de estructuras de proteínas del Protein Data Bank. Al iniciar el programa, se despliega un menú básico vía terminal, el cúal contiene opciones para ingresar, eliminar o reemplazar una ID, cargar un archivo de texto con las IDs, generar un grafo de los datos ingresados, o salir del programa. Según la definición de lo que se considera un árbol balanceado, este algoritmo tiene la capacidad de ordenar los datos ingresados según su factor de equilibrio (FE), ordenando los datos por orden alfabético.
Además de lo anterior, esta guía cuenta con 7 archivos de texto con alrededor de 170.000 IDs, una ID por cada línea, y un archivo de texto adicional con no más de 10 IDs que fué utilizado para probar el algoritmo (ya que los demás archivos eran demasiado grandes para ser cargados en el programa).  

### Ejecución
El algoritmo cuenta con su makefile, por lo que con el comando "make" se generará un ejecutable llamado "programa", el cuál se debe ejecutar con el comando "./programa", lo que permitirá el usuario iniciar el algoritmo.

### Construido con
El presente proyecto de programación, se construyó en base al lenguaje de programación C++, en conjunto con el IDE "Visual Studio Code".

* [C++] - Lenguaje de programación utilizado.
* [Visual Studio Code](https://code.visualstudio.com/) - IDE utilizado.

### Autores
* **Daniel Tobar** - Estudiante de Ingeniería Civil en Bioinformática.
