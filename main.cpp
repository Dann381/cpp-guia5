#include <fstream>
#include <string>
#include <cctype>
#include <iostream>
using namespace std;

//Estructura del nodo, con información tipo string y factor de equilibrio
typedef struct _Nodo {
    struct _Nodo *izq;
    struct _Nodo *der;
    string info;
    int FE;
} Nodo;

//Clase para crear imagen del grafo
class Grafo {
    private:
    public:
        Grafo (Nodo *nodo) {
            ofstream fp;
            string aux,aux1;
            aux = '"' + nodo->info + '"';
            fp.open ("grafo.txt");
            fp << "digraph G {" << endl;
            fp << "node [style=filled fillcolor=yellow];" << endl;
            fp << "nullraiz [shape=point];" << endl;
            fp << "nullraiz->" + aux + " [label=" << nodo->FE << "];"<< endl; 
            recorrer(nodo, fp);
            fp << "}" << endl;
            fp.close();
            system("dot -Tpng -ografo.png grafo.txt &");
            system("eog grafo.png &");
        }
        void recorrer(Nodo *p, ofstream &fp) {
            string cadena = "\0";    
            if (p != NULL) {
                if (p->izq != NULL) {
                    fp << '"' <<  p->info << '"' << "->" << '"' << p->izq->info << '"' << " [label=" << p->izq->FE << "];" << endl;
                } else {      
                    fp << '"' << p->info << "i" << '"' << " [shape=point];" << endl;
                    fp << '"' << p->info << '"' << "->" << '"' << p->info << "i" << '"' << endl;
                }
                if (p->der != NULL) { 
                    fp << '"' <<  p->info << '"' << "->" << '"' << p->der->info << '"' << " [label=" << p->der->FE << "];" << endl;
                } else {
                    fp << '"' << p->info << "d" << '"' << " [shape=point];" << endl;
                    fp << '"' << p->info << '"' << "->" << '"' << p->info << "d" << '"' << endl;
                }
                recorrer(p->izq, fp);
                recorrer(p->der, fp); 
            }
        }
};

//Función para buscar si existe un dato en el árbol o no
int Busqueda(Nodo *nodo,string info, int (&esta)) {
    if (nodo != NULL) {
        if (info < nodo->info) {
        Busqueda(nodo->izq, info, esta);
        } 
        else {
            if (info > nodo->info) {
            Busqueda(nodo->der,info, esta);
            } 
            else {
                printf("El nodo SI se encuentra en el árbol\n");
                esta = 1;
            }
        }
    } 
    else {
        printf("El nodo NO se encuentra en el árbol\n");
        esta = 2;
    }
    return esta;
}

//Función con las opciones básicas del menú
int menu(){
    int opt;
    cout << "-------------- Menú --------------" << endl;
    cout << "[1] Agregar código" << endl;
    cout << "[2] Eliminar código" << endl;
    cout << "[3] Modificar código" << endl;
    cout << "[4] Cargar archivo" << endl;
    cout << "[5] Generar un grafo del árbol" << endl;
    cout << "[6] Salir" << endl;
    cout << "Opción: ";
    cin >> opt;
    system("clear");
    return opt;
}

//Función para insertar un código y ordenar según FE
void InsercionBalanceado(Nodo **nodocabeza, int *BO, string info){
    Nodo *nodo = NULL;
    Nodo *nodo1 = NULL;
    Nodo *nodo2 = NULL; 
    nodo = *nodocabeza;
    if (nodo != NULL){
        if (info < nodo->info){
            InsercionBalanceado(&(nodo->izq), BO, info);
            if(*BO == true){
                switch (nodo->FE) {
                    case 1:
                        nodo->FE = 0;
                        *BO = false;
                        break;
                    case 0:
                        nodo->FE = -1;
                        break;
                    case -1: 
                        /* reestructuración del árbol */
                        nodo1 = nodo->izq;
                        /* Rotacion II */
                        if (nodo1->FE <= 0){ 
                            nodo->izq = nodo1->der;
                            nodo1->der = nodo;
                            nodo->FE = 0;
                            nodo = nodo1;
                        } 
                        else { 
                        /* Rotacion ID */
                            nodo2 = nodo1->der;
                            nodo->izq = nodo2->der;
                            nodo2->der = nodo;
                            nodo1->der = nodo2->izq;
                            nodo2->izq = nodo1;
                            if (nodo2->FE == -1){
                                nodo->FE = 1;
                            }
                            else{
                                nodo->FE =0;
                            }
                            if (nodo2->FE == 1){
                                nodo1->FE = -1;
                            }
                            else{
                                nodo1->FE = 0;
                            }
                            nodo = nodo2;
                        }
                        nodo->FE = 0;
                        *BO = false;
                        break;    
                }
            }
        }
        else {
            if (info > nodo->info) {
                InsercionBalanceado(&(nodo->der), BO, info);
                if (*BO == true) {
                    switch (nodo->FE) {
                        case -1:
                            nodo->FE = 0;
                            *BO = false;
                            break;
                        case 0:
                            nodo->FE = 1;
                            break;
                        case 1: 
                            /* reestructuración del árbol */
                            nodo1 = nodo->der;
                            if (nodo1->FE >= 0) { 
                                /* Rotacion DD */
                                nodo->der = nodo1->izq;
                                nodo1->izq = nodo;
                                nodo->FE = 0;
                                nodo = nodo1;
                            }
                            else { 
                                /* Rotacion DI */
                                nodo2 = nodo1->izq;
                                nodo->der = nodo2->izq;
                                nodo2->izq = nodo;
                                nodo1->izq = nodo2->der;
                                nodo2->der = nodo1;
                                if (nodo2->FE == 1){
                                    nodo->FE = -1;
                                }   
                                else{
                                    nodo->FE = 0;
                                }       
                                if (nodo2->FE == -1){
                                    nodo1->FE = 1;
                                }
                                else{
                                    nodo1->FE = 0;
                                }
                                nodo = nodo2;
                            }
                            nodo->FE = 0;
                            *BO = false;
                            break;
                    }
                }
            } 
            else{
                printf("El nodo ya se encuentra en el árbol\n");
            }
            
        }
    }
    else {
        nodo = (struct _Nodo*) malloc (sizeof(Nodo));
        nodo->izq = NULL;
        nodo->der = NULL;
        nodo->info = info;
        nodo->FE = 0;
        *BO = true;
    }
    *nodocabeza = nodo;
}

//Función que reemplaza un nodo hoja por uno de sus hijos (NULL) para poder borrarlo
void reemplazar(Nodo** act){
    Nodo* a, *p;
    p = *act;

    a = (*act)->izq;
    while(a->der){
        p = a;
        a = a->der;
    }
    (*act)->info = a->info;
    if (p == (*act)){
        p->izq = a->izq;
    }
    else{
        p->der = a->izq;
    }
    (*act) = a;
}

void Restructura1(Nodo **nodocabeza, int *BO) {
    Nodo *nodo, *nodo1, *nodo2; 
    nodo = *nodocabeza;   
    if (*BO == true){
        switch (nodo->FE){
            case -1:
                nodo->FE = 0;
                break;
            case 0:
                nodo->FE = 1;
                *BO = false;
                break;
            case 1:
            /* reestructuracion del árbol */
                nodo1 = nodo->der;
                if (nodo1->FE >= 0) {
                    /* rotacion DD */
                    nodo->der = nodo1->izq;
                    nodo1->izq = nodo;
                    switch (nodo1->FE) {
                        case 0:
                            nodo->FE = 1;
                            nodo1->FE = -1;
                            *BO = false;
                            break;
                        case 1:
                            nodo->FE = 0;
                            nodo1->FE = 0;
                            *BO = false;
                            break;
                    }
                    nodo = nodo1;
                } 
                else {
                    /* Rotacion DI */
                    nodo2 = nodo1->izq;
                    nodo->der = nodo2->izq;
                    nodo2->izq = nodo;
                    nodo1->izq = nodo2->der;
                    nodo2->der = nodo1;
                    if (nodo2->FE == 1){
                        nodo->FE = -1;
                    }
                    else{
                        nodo->FE = 0;
                    }
                    if (nodo2->FE == -1){
                        nodo1->FE = 1;
                    }
                    else{
                        nodo1->FE = 0;
                    }
                    nodo = nodo2;
                    nodo2->FE = 0;       
                } 
            break;   
        }
    }
    *nodocabeza=nodo;
}

void Restructura2(Nodo **nodocabeza, int *BO){
    Nodo *nodo, *nodo1, *nodo2;
    nodo = *nodocabeza;
    if (*BO == true){
        switch (nodo->FE) {
            case 1:
                nodo->FE = 0;
                break;
            case 0:
                nodo->FE = -1;
                *BO = false;
                break;
            case -1:
                /* reestructuracion del árbol */
                nodo1 = nodo->izq;
                if (nodo1->FE<=0) {
                    /* rotacion II */
                    nodo->izq = nodo1->der;
                    nodo1->der = nodo;
                    switch (nodo1->FE) {
                        case 0:
                            nodo->FE = -1;
                            nodo1->FE = 1;
                            *BO = false;
                            break;
                        case -1:
                            nodo->FE = 0;
                            nodo1->FE = 0;
                            *BO = false;
                            break;
                    }
                    nodo = nodo1;
                } 
                else { 
                    /* Rotacion ID */
                    nodo2 = nodo1->der;
                    nodo->izq = nodo2->der;
                    nodo2->der = nodo;
                    nodo1->der = nodo2->izq;
                    nodo2->izq = nodo1;
                    if (nodo2->FE == -1){
                        nodo->FE = 1;
                    }
                    else{
                        nodo->FE = 0;
                    }
                    if (nodo2->FE == 1){
                        nodo1->FE = -1;
                    }
                    else{
                        nodo1->FE = 0;
                    }
                    nodo = nodo2;
                    nodo2->FE = 0;
                }
            break;
        }
    }
    *nodocabeza = nodo;
}

//Borra un nodo y reestructura el árbol
void Borra(Nodo **aux1, Nodo **otro1, int *BO){
    Nodo *aux, *otro; 
    aux= *aux1;
    otro=*otro1; 
    if (aux->der != NULL){
        Borra(&(aux->der), &otro, BO);
        Restructura2(&aux, BO);
    } 
    else{
        otro->info = aux->info;
        aux = aux->izq;
        *BO = true;
    }
    *aux1=aux;
    *otro1=otro;
}

//Función que busca un nodo, lo elimina y reestructura el árbol
void EliminacionBalanceado(Nodo **nodocabeza, int *BO, string info){
    Nodo *nodo, *otro; 
    nodo = *nodocabeza;
    if (nodo != NULL){
        if (info < nodo->info){
            EliminacionBalanceado(&(nodo->izq), BO, info);
            Restructura1(&nodo, BO);
        } 
        else{
            if (info > nodo->info){
                EliminacionBalanceado(&(nodo->der), BO, info);
                Restructura2(&nodo, BO);
            } 
            else{
                otro = nodo;
                if (otro->der == NULL){
                    nodo = otro->izq;
                    *BO = true;   
                } 
                else{
                    if (otro->izq==NULL){
                        nodo=otro->der;
                        *BO=true; 
                    }
                    else if (otro->izq == NULL && otro->der == NULL){
                        Borra(&(otro->izq),&otro,BO);
                        Restructura1(&nodo,BO);
                        free(otro);
                    }
                    else{
                        reemplazar(&otro);
                        free(otro);
                        Restructura1(&nodo, BO);
                    }
                } 
            }
        }
    }
    else {
        printf("El nodo NO se encuentra en el árbol\n");
    }
    *nodocabeza=nodo;
}


// Función principal
int main(void) {
    system("clear");
    Nodo *raiz = NULL;
    string info;
    system("clear");
    int inicio;
    int esta;
    int opt = 0;
    while (opt != 6){
        opt = menu();
        if(opt == 1){
            system("clear");
            cin.ignore();
            cout << "Ingrese un código para insertar: ";
            getline(cin, info);
            inicio = false;
            InsercionBalanceado((&raiz), (&inicio), info);
        }
        else if(opt == 2){
            system("clear");
            cin.ignore();
            cout << "Ingrese un código para eliminar: ";
            getline(cin, info);
            inicio = false;
            EliminacionBalanceado((&raiz), (&inicio), info);
        }
        else if(opt == 3){
            system("clear");
            cin.ignore();
            cout << "Ingrese un código para modificar: ";
            getline(cin, info);
            esta = Busqueda(raiz, info, esta);
            if (esta == 1){
                inicio = false;
                EliminacionBalanceado((&raiz), (&inicio), info);
                cin.ignore();
                cout << "Ingrese un nuevo número para agregar: ";
                getline(cin, info);
                inicio = false;
                InsercionBalanceado((&raiz), (&inicio), info);
            }   
        }
        else if(opt == 4){
            system("clear");
            string archivo;
            cin.ignore();
            cout << "Ingrese nombre del archivo: ";
            getline(cin, archivo);
            ifstream f;
            f.open(archivo);
            if(f.is_open()){
                string linea;
                inicio = false;
                while(getline(f,linea)){
                    InsercionBalanceado((&raiz), (&inicio), linea);
                }
                cout << "Archivo cargado correctamente" << endl;
                f.close();
            }
            else{
                cout << "El archivo ingresado no existe" << endl;
            }
            
        }
        else if(opt == 5){
            system("clear");
            Grafo *g = new Grafo(raiz);
            cout << "Grafo del árbol generado" << endl;
        }
        else if(opt == 6){
            system("clear");
            cout << "Saliendo..." << endl;
        }
        else{
            system("clear");
            cout << "Ingrese un número válido" << endl;
        }
    }
    
    return 0;
}
